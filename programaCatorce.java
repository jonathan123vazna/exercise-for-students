/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg95programasdejava;

import java.util.Scanner;

/**
 *
 * @author jonathandev
 */
public class programaCatorce {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner entrada = new Scanner(System.in);
        int numero;

        System.out.println("Ingrese un numero");
        numero = entrada.nextInt();

        System.out.println("Iniciando cuenta regresiva");
        for (int i = 0; numero > i; numero--) {
            System.out.println(numero);
        }

    }

}

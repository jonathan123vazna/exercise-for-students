/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programasescuela;
import java.util.Scanner;
/**
 *
 * @author ing_o
 */
public class programa18 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int temp;
        Scanner entrada = new Scanner(System.in);
        
        System.out.println("Ingrese un temperatura en rango 0 - 40");
        temp = entrada.nextInt();
        
        if(temp >= 0 && temp <= 10){
            System.out.println("Mucho Frio");
        }else if(temp >= 11 && temp <= 18){
            System.out.println("Frio");
        }else if(temp >= 19 && temp <= 25){
            System.out.println("Templado");
        }else if(temp >= 26 && temp <= 40){
            System.out.println("Mucho Calor");
            
        }else{
            System.out.println("No se encuentra en el rango establecido");
        }
    }
    
}

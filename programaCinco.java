/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg95programasdejava;

import java.util.Scanner;

/**
 *
 * @author jonathandev
 */
public class programaCinco {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        int baseMay, baseMen, area;
        
        System.out.println("Ingrese la base Mayor");
        baseMay = entrada.nextInt();
        
        System.out.println("Ingrese la base Menor");
        baseMen = entrada.nextInt();
        
        area = (baseMay * baseMen) / 2;
        
        System.out.println("El area es: " + area);
    }
    
}

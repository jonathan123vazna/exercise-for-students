/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programasescuela;
import java.util.Scanner;
/**
 *
 * @author ing_o
 */
public class programa20 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String letra;
        Scanner entrada = new Scanner(System.in);
        
        System.out.println("Linea de autobuses ADO, seleccione su destino: ");
        System.out.println("a) Puebla. [$150.00]\n" +
                           "b) Veracruz. [$320.00]\n" +
                           "c) Tlaxcala. [$120.00]\n" +
                           "d) Cuernavaca. [$90.00]\n" +
                           "e) Queretaro.[$110.00]");
        letra = entrada.next();
        switch(letra){
            case "a":
                System.out.println("Su destino es “PUEBLA”, tiempo de traslado 2 hrs, costo $200.00 ");
                break;
            case "b":
                System.out.println("Su destino es “VERACRUZ”, tiempo de traslado 5 hrs, costo $350.00 ");
                break;
            case "c":
                System.out.println("Su destino es “TLAXCALA”, tiempo de traslado 3 hrs, costo $240.50 ");
                break;
            case "d":
                System.out.println("Su destino es “CUERNAVACA”, tiempo de traslado 1:30 hrs, costo $150.00 ");
                break;
            case "e":
                System.out.println("Su destino es “QUERETARO”, tiempo de traslado 1 hrs, costo $230.00 ");
                break;
            default:
                System.out.println("Destino Invalido");
        }
    }
    
}

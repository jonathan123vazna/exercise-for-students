/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg95programasdejava;

import java.util.Scanner;

/**
 *
 * @author jonathandev
 */
public class programaCuatro {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        float radio, op;
        
        System.out.println("Ingrese radio");
        radio = entrada.nextFloat();
        
        op = (float) (3.14159 * radio * radio);
        
        System.out.println("El area es: " + op);
    }
    
}

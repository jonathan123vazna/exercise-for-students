/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programasescuela;
import java.util.Scanner;
/**
 *
 * @author ing_o
 */
public class programa19 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int valor;
        Scanner entrada = new Scanner(System.in);
        
        System.out.println("Seleccione un numero de 1 - 7");
        valor = entrada.nextInt();
        
        switch(valor){
            case 1:
                System.out.println("Lunes");
                break;
            case 2:
                System.out.println("Martes");
                break;
            case 3:
                System.out.println("Miercoles");
                break;
            case 4:
                System.out.println("Jueves");
                break;
            case 5:
                System.out.println("Viernes");
                break;
            case 6:
                System.out.println("Sabado");
                break;
            case 7:
                System.out.println("Domingo");
                break;
            default:
                System.out.println("No se encuetra en el rango");
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg95programasdejava;

import java.util.Scanner;

/**
 *
 * @author jonathandev
 */
public class programaTres {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int base,altura, op;
        
        Scanner entrada = new Scanner(System.in);  //Constructor para el scanner del teclado
        
        //Agregando base y altura
        System.out.println("Ingrese la base");
        base = entrada.nextInt();
        
        System.out.println("Ingrese la altura");
        altura = entrada.nextInt();
        
        //Realizando operacion
        op = base * altura / 2;
        
        //Mostrando resultado
        System.out.println("El area es: " + op);
        
        
    }
    
}

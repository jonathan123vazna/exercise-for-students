/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg95programasdejava;

import java.util.Scanner;

/**
 *
 * @author jonathandev
 */
public class programaOcho {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Scanner entrada = new Scanner(System.in);
       double a, b, op;
       
       System.out.println("Ingrese su primer valor");
       a = entrada.nextDouble();
       
       System.out.println("Ingrese su segundo valor");
       b = entrada.nextDouble();
       
       //Operacion con % para obtener el residuo
       op = (a%b);
       
       System.out.println("El residuo de la operacion es: " + op);
       
    }
    
}
